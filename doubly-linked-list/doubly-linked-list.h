#pragma once
#include <Windows.h>

// Function pointer definitions
typedef VOID(WINAPI *CLEANUP_FUNC)(LPVOID);
typedef BOOL(WINAPI *COMPARISON_FUNC)(LPVOID, LPVOID);

// Helper data that is used for the list
typedef struct _LIST_HELPER {
	CLEANUP_FUNC CleanData;
	COMPARISON_FUNC CompareData;
	CRITICAL_SECTION ListLock;
	HANDLE ListHeap;
} LIST_HELPER, *PLIST_HELPER;

// A single node in the list
typedef struct _LIST_NODE {
	struct _LIST_NODE *plnNext;
	struct _LIST_NODE *plnPrev;
	LPVOID lpData;
} LIST_NODE, *PLIST_NODE;


// The main list structure
typedef struct _LIST {
	PLIST_NODE plnHead;
	PLIST_NODE plnTail;
	SIZE_T sztLen;
	LIST_HELPER Helpers;
} LIST, *PLIST;


// Initialization functions
BOOL ListInitialize(PLIST *plList, CLEANUP_FUNC pfnClean, COMPARISON_FUNC pfnCompare);

// Helper functions
BOOL ListIsEmpty(PLIST plList);
BOOL ListSearch(PLIST plList, LPVOID lpSearchData, LPVOID *lpRetData);

// Insertion functions
BOOL ListInsertHead(PLIST plList, LPVOID lpData);
BOOL ListInsertTail(PLIST plList, LPVOID lpData);

// Removal functions
BOOL ListRemoveHead(PLIST plList, LPVOID *lpData);
BOOL ListRemoveTail(PLIST plList, LPVOID *lpData);

// Deletion Functions
BOOL ListDeleteHead(PLIST plList);
BOOL ListDeleteTail(PLIST plList);
BOOL ListDeleteAll(PLIST plList);
BOOL ListCleanup(PLIST plList);