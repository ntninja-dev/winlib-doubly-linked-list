#include "doubly-linked-list.h"

BOOL
ListInitialize(PLIST *plList, CLEANUP_FUNC pfnClean, COMPARISON_FUNC pfnCompare)
{
	BOOL bRetVal = FALSE;
	HANDLE hNewListHeap = NULL;
	PLIST plNewList = NULL;
	
	if (NULL == plList || NULL == pfnClean || NULL == pfnCompare) {
		goto cleanup;
	}

	hNewListHeap = HeapCreate(0, 0, 0);
	if (NULL == hNewListHeap) {
		goto cleanup;
	}

	plNewList = HeapAlloc(hNewListHeap, HEAP_ZERO_MEMORY, sizeof(LIST));
	if (NULL == plNewList) {
		goto cleanup_heap;
	}

	InitializeCriticalSection(&plNewList->Helpers.ListLock);
	plNewList->Helpers.CleanData = pfnClean;
	plNewList->Helpers.CompareData = pfnCompare;
	plNewList->Helpers.ListHeap = hNewListHeap;

	plNewList->plnHead = NULL;
	plNewList->plnTail = NULL;
	plNewList->sztLen = 0;

	*plList = plNewList;
	bRetVal = TRUE;
	goto cleanup;

cleanup_heap:
	HeapDestroy(hNewListHeap);
cleanup:
	return bRetVal;
}

BOOL
ListIsEmpty(PLIST plList)
{
	return (0 == plList->sztLen);
}

BOOL
ListSearch(PLIST plList, LPVOID lpSearchData, LPVOID *lpRetData)
{
	BOOL bRetVal = FALSE;
	PLIST_NODE plnCurrNode = NULL;

	if (NULL == plList || TRUE == ListIsEmpty(plList)) {
		goto cleanup;
	}

	EnterCriticalSection(&plList->Helpers.ListLock);
	plnCurrNode = plList->plnHead;

	do {
		bRetVal = plList->Helpers.CompareData(lpSearchData, plnCurrNode->lpData);
		if (TRUE == bRetVal) {
			*lpRetData = plnCurrNode->lpData;
		}

		plnCurrNode = plnCurrNode->plnNext;
	} while (NULL != plnCurrNode && FALSE == bRetVal);
	
	LeaveCriticalSection(&plList->Helpers.ListLock);
	
cleanup:
 	return bRetVal;
}

BOOL 
ListInsertHead(PLIST plList, LPVOID lpData)
{
	BOOL bRetVal = FALSE;
	PLIST_NODE plnNewNode = FALSE;

	if (NULL == plList) {
		goto cleanup;
	}

	plnNewNode = HeapAlloc(plList->Helpers.ListHeap, HEAP_ZERO_MEMORY, sizeof(LIST_NODE));
	if (NULL == plnNewNode) {
		goto cleanup;
	}

	plnNewNode->lpData = lpData;
	plnNewNode->plnNext = NULL;
	plnNewNode->plnPrev = NULL;

	EnterCriticalSection(&plList->Helpers.ListLock);

	if (TRUE == ListIsEmpty(plList)) {
		plList->plnHead = plnNewNode;
		plList->plnTail = plnNewNode;
	}
	else {
		plList->plnHead->plnPrev = plnNewNode;
		plnNewNode->plnNext = plList->plnHead;
		plList->plnHead = plnNewNode;
	}

	plList->sztLen++;
	LeaveCriticalSection(&plList->Helpers.ListLock);

	bRetVal = TRUE;

cleanup:
	return bRetVal;
}

BOOL
ListInsertTail(PLIST plList, LPVOID lpData)
{
	BOOL bRetVal = FALSE;
	PLIST_NODE plnNewNode = FALSE;

	if (NULL == plList) {
		goto cleanup;
	}

	plnNewNode = HeapAlloc(plList->Helpers.ListHeap, HEAP_ZERO_MEMORY, sizeof(LIST_NODE));
	if (NULL == plnNewNode) {
		goto cleanup;
	}

	plnNewNode->lpData = lpData;
	plnNewNode->plnNext = NULL;
	plnNewNode->plnPrev = NULL;

	EnterCriticalSection(&plList->Helpers.ListLock);

	if (TRUE == ListIsEmpty(plList)) {
		plList->plnHead = plnNewNode;
		plList->plnTail = plnNewNode;
	}
	else {
		plList->plnTail->plnNext = plnNewNode;
		plnNewNode->plnPrev = plList->plnTail;
		plList->plnTail = plnNewNode;
	}

	plList->sztLen++;
	LeaveCriticalSection(&plList->Helpers.ListLock);

	bRetVal = TRUE;

cleanup:
	return bRetVal;
}

BOOL
ListRemoveHead(PLIST plList, LPVOID *lpData)
{
	BOOL bRetVal = FALSE;
	PLIST_NODE plnRemovedNode = NULL;

	if (NULL == plList || TRUE == ListIsEmpty(plList)) {
		goto cleanup;
	}

	EnterCriticalSection(&plList->Helpers.ListLock);

	plnRemovedNode = plList->plnHead;
	plList->sztLen--;

	if (TRUE == ListIsEmpty(plList)) {
		plList->plnHead = NULL;
		plList->plnTail = NULL;
	}
	else {
		plnRemovedNode->plnNext->plnPrev = NULL;
		plList->plnHead = plnRemovedNode->plnNext;
	}

	LeaveCriticalSection(&plList->Helpers.ListLock);

	*lpData = plnRemovedNode->lpData;
	HeapFree(plList->Helpers.ListHeap, 0, plnRemovedNode);
	bRetVal = TRUE;

cleanup:
	return bRetVal;
}

BOOL 
ListRemoveTail(PLIST plList, LPVOID *lpData)
{
	BOOL bRetVal = FALSE;
	PLIST_NODE plnRemovedNode = NULL;

	if (NULL == plList || TRUE == ListIsEmpty(plList)) {
		goto cleanup;
	}

	EnterCriticalSection(&plList->Helpers.ListLock);

	plnRemovedNode = plList->plnTail;
	plList->sztLen--;

	if (TRUE == ListIsEmpty(plList)) {
		plList->plnHead = NULL;
		plList->plnTail = NULL;
	}
	else {
		plnRemovedNode->plnPrev->plnNext = NULL;
		plList->plnTail = plnRemovedNode->plnPrev;
	}

	LeaveCriticalSection(&plList->Helpers.ListLock);

	*lpData = plnRemovedNode->lpData;
	HeapFree(plList->Helpers.ListHeap, 0, plnRemovedNode);
	bRetVal = TRUE;

cleanup:
	return bRetVal;
}

BOOL
ListDeleteHead(PLIST plList)
{
	BOOL bRetVal = FALSE;
	LPVOID lpData = NULL;

	bRetVal = ListRemoveHead(plList, &lpData);
	if (FALSE == bRetVal) {
		goto cleanup;
	}

	plList->Helpers.CleanData(lpData);
	bRetVal = TRUE;
cleanup:
	return bRetVal;
}

BOOL
ListDeleteTail(PLIST plList)
{
	BOOL bRetVal = FALSE;
	LPVOID lpData = NULL;

	bRetVal = ListRemoveTail(plList, &lpData);
	if (FALSE == bRetVal) {
		goto cleanup;
	}

	plList->Helpers.CleanData(lpData);
	bRetVal = TRUE;
cleanup:
	return bRetVal;
}

BOOL
ListDeleteAll(PLIST plList)
{
	BOOL bRetVal = FALSE;
	LPVOID lpData = NULL;

	while (FALSE == ListIsEmpty(plList)) {
		bRetVal = ListRemoveHead(plList, &lpData);
		if (FALSE == bRetVal) {
			goto cleanup;
		}

		plList->Helpers.CleanData(lpData);
	}

	bRetVal = TRUE;
cleanup:
	return bRetVal;
}

BOOL 
ListCleanup(PLIST plList)
{
	HANDLE hListHeap = NULL;

	ListDeleteAll(plList);
	DeleteCriticalSection(&plList->Helpers.ListLock);
	
	hListHeap = plList->Helpers.ListHeap;
	HeapFree(hListHeap, 0, plList);
	HeapDestroy(hListHeap);

	return TRUE;
}