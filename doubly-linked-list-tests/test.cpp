#include "pch.h"

extern "C" {
#include "../doubly-linked-list/doubly-linked-list.h"
}

VOID WINAPI 
TestClean(LPVOID lpData)
{
	lpData = NULL;
	return;
}

BOOL WINAPI
TestCompare(LPVOID lpData, LPVOID lpCompare)
{
	return (lpData == lpCompare);
}

class DoublyLinkedListTesting : public ::testing::Test {

	protected:
		PLIST TestList = NULL;

		VOID SetUp() override {
			ListInitialize(&TestList, TestClean, TestCompare);
			return;
		}

		VOID TearDown() override {
			ListCleanup(TestList);
			return;
		}
};

TEST(DoublyLinkedListTestPreFixture, TestInitialization)
{
	PLIST TestList = NULL;
	BOOL bRetVal = FALSE;
	
	bRetVal = ListInitialize(NULL, NULL, NULL);
	EXPECT_EQ(FALSE, bRetVal);

	bRetVal = ListInitialize(&TestList, NULL, NULL);
	EXPECT_EQ(FALSE, bRetVal);

	bRetVal = ListInitialize(&TestList, TestClean, NULL);
	EXPECT_EQ(FALSE, bRetVal);

	bRetVal = ListInitialize(&TestList, NULL, TestCompare);
	EXPECT_EQ(FALSE, bRetVal);

	bRetVal = ListInitialize(&TestList, TestClean, TestCompare);
	EXPECT_EQ(TRUE, bRetVal);
	EXPECT_EQ((SIZE_T)0, TestList->sztLen);
}

TEST(DoublyLinkedListTestPreFixture, TestListIsEmpty)
{
	PLIST TestList = NULL;
	BOOL bRetVal = FALSE;

	bRetVal = ListInitialize(&TestList, TestClean, TestCompare);

	bRetVal = ListIsEmpty(TestList);
	EXPECT_EQ(TRUE, bRetVal);
	EXPECT_EQ((SIZE_T)0, TestList->sztLen);

	TestList->sztLen = 1;
	bRetVal = ListIsEmpty(TestList);
	EXPECT_EQ(FALSE, bRetVal);
}

TEST(DoublyLinkedListTestPreFixture, TestCleanup)
{
	PLIST TestList = NULL;
	BOOL bRetVal = FALSE;

	bRetVal = ListInitialize(&TestList, TestClean, TestCompare);
	EXPECT_EQ(TRUE, bRetVal);

	bRetVal = ListCleanup(TestList);
	EXPECT_EQ(TRUE, bRetVal);
}

TEST_F(DoublyLinkedListTesting, TestListInsertHead) {
	
	ListInsertHead(TestList, (LPVOID)0);
	EXPECT_EQ(0, (INT)TestList->plnHead->lpData);
	EXPECT_EQ(0, (INT)TestList->plnTail->lpData);
	EXPECT_EQ((SIZE_T)1, TestList->sztLen);

	ListInsertHead(TestList, (LPVOID)1);
	EXPECT_EQ(1, (INT)TestList->plnHead->lpData);
	EXPECT_EQ(0, (INT)TestList->plnTail->lpData);
	EXPECT_EQ((SIZE_T)2, TestList->sztLen);

	ListInsertHead(TestList, (LPVOID)2);
	EXPECT_EQ(2, (INT)TestList->plnHead->lpData);
	EXPECT_EQ(0, (INT)TestList->plnTail->lpData);
	EXPECT_EQ((SIZE_T)3, TestList->sztLen);
}

TEST_F(DoublyLinkedListTesting, TestListInsertTail) {

	ListInsertTail(TestList, (LPVOID)0);
	EXPECT_EQ(0, (INT)TestList->plnTail->lpData);
	EXPECT_EQ(0, (INT)TestList->plnHead->lpData);
	EXPECT_EQ((SIZE_T)1, TestList->sztLen);

	ListInsertTail(TestList, (LPVOID)1);
	EXPECT_EQ(1, (INT)TestList->plnTail->lpData);
	EXPECT_EQ(0, (INT)TestList->plnHead->lpData);
	EXPECT_EQ((SIZE_T)2, TestList->sztLen);

	ListInsertTail(TestList, (LPVOID)2);
	EXPECT_EQ(2, (INT)TestList->plnTail->lpData);
	EXPECT_EQ(0, (INT)TestList->plnHead->lpData);
	EXPECT_EQ((SIZE_T)3, TestList->sztLen);
}

TEST_F(DoublyLinkedListTesting, TestListSearch) {
	BOOL bRetVal = FALSE;
	LPVOID lpRet = NULL;

	ListInsertHead(TestList, (LPVOID)0);
	ListInsertHead(TestList, (LPVOID)1);
	ListInsertHead(TestList, (LPVOID)2);
	ListInsertHead(TestList, (LPVOID)3);
	ListInsertHead(TestList, (LPVOID)4);
	ListInsertHead(TestList, (LPVOID)5);
	ListInsertHead(TestList, (LPVOID)6);
	ListInsertHead(TestList, (LPVOID)7);

	bRetVal = ListSearch(TestList, (LPVOID)4, &lpRet);
	EXPECT_EQ(TRUE, bRetVal);
	EXPECT_EQ(4, (INT)lpRet);

	bRetVal = ListSearch(TestList, (LPVOID)0, &lpRet);
	EXPECT_EQ(TRUE, bRetVal);
	EXPECT_EQ(0, (INT)lpRet);

	bRetVal = ListSearch(TestList, (LPVOID)7, &lpRet);
	EXPECT_EQ(TRUE, bRetVal);
	EXPECT_EQ(7, (INT)lpRet);

	bRetVal = ListSearch(TestList, (LPVOID)10, &lpRet);
	EXPECT_EQ(FALSE, bRetVal);
}

TEST_F(DoublyLinkedListTesting, TestRemoveHead) {
	BOOL bRetVal = FALSE;
	LPVOID lpRet = NULL;

	bRetVal = ListRemoveHead(TestList, &lpRet);
	EXPECT_EQ(FALSE, bRetVal);

	ListInsertHead(TestList, (LPVOID)0);
	ListInsertHead(TestList, (LPVOID)1);
	ListInsertHead(TestList, (LPVOID)2);
	ListInsertHead(TestList, (LPVOID)3);
	ListInsertHead(TestList, (LPVOID)4);
	ListInsertHead(TestList, (LPVOID)5);
	ListInsertHead(TestList, (LPVOID)6);
	ListInsertHead(TestList, (LPVOID)7);

	bRetVal = ListRemoveHead(TestList, &lpRet);
	EXPECT_EQ(TRUE, bRetVal);
	EXPECT_EQ(7, (INT)lpRet);
	EXPECT_EQ((SIZE_T)7, TestList->sztLen);

	bRetVal = ListRemoveHead(TestList, &lpRet);
	EXPECT_EQ(TRUE, bRetVal);
	EXPECT_EQ(6, (INT)lpRet);
	EXPECT_EQ((SIZE_T)6, TestList->sztLen);

	bRetVal = ListRemoveHead(TestList, &lpRet);
	bRetVal = ListRemoveHead(TestList, &lpRet);
	bRetVal = ListRemoveHead(TestList, &lpRet);
	bRetVal = ListRemoveHead(TestList, &lpRet);
	bRetVal = ListRemoveHead(TestList, &lpRet);
	bRetVal = ListRemoveHead(TestList, &lpRet);
	EXPECT_EQ(TRUE, bRetVal);
	EXPECT_EQ(0, (INT)lpRet);
	EXPECT_EQ((SIZE_T)0, TestList->sztLen);
	EXPECT_EQ(TRUE, ListIsEmpty(TestList));
	EXPECT_EQ(NULL, TestList->plnHead);
	EXPECT_EQ(NULL, TestList->plnTail);

	bRetVal = ListRemoveHead(TestList, &lpRet);
	EXPECT_EQ(FALSE, bRetVal);
}

TEST_F(DoublyLinkedListTesting, TestRemoveTail) {
	BOOL bRetVal = FALSE;
	LPVOID lpRet = NULL;

	bRetVal = ListRemoveTail(TestList, &lpRet);
	EXPECT_EQ(FALSE, bRetVal);

	ListInsertHead(TestList, (LPVOID)0);
	ListInsertHead(TestList, (LPVOID)1);
	ListInsertHead(TestList, (LPVOID)2);
	ListInsertHead(TestList, (LPVOID)3);
	ListInsertHead(TestList, (LPVOID)4);
	ListInsertHead(TestList, (LPVOID)5);
	ListInsertHead(TestList, (LPVOID)6);
	ListInsertHead(TestList, (LPVOID)7);

	bRetVal = ListRemoveTail(TestList, &lpRet);
	EXPECT_EQ(TRUE, bRetVal);
	EXPECT_EQ(0, (INT)lpRet);
	EXPECT_EQ((SIZE_T)7, TestList->sztLen);

	bRetVal = ListRemoveTail(TestList, &lpRet);
	EXPECT_EQ(TRUE, bRetVal);
	EXPECT_EQ(1, (INT)lpRet);
	EXPECT_EQ((SIZE_T)6, TestList->sztLen);

	bRetVal = ListRemoveTail(TestList, &lpRet);
	bRetVal = ListRemoveTail(TestList, &lpRet);
	bRetVal = ListRemoveTail(TestList, &lpRet);
	bRetVal = ListRemoveTail(TestList, &lpRet);
	bRetVal = ListRemoveTail(TestList, &lpRet);
	bRetVal = ListRemoveTail(TestList, &lpRet);
	EXPECT_EQ(TRUE, bRetVal);
	EXPECT_EQ(7, (INT)lpRet);
	EXPECT_EQ((SIZE_T)0, TestList->sztLen);
	EXPECT_EQ(TRUE, ListIsEmpty(TestList));
	EXPECT_EQ(NULL, TestList->plnHead);
	EXPECT_EQ(NULL, TestList->plnTail);

	bRetVal = ListRemoveTail(TestList, &lpRet);
	EXPECT_EQ(FALSE, bRetVal);
}

TEST_F(DoublyLinkedListTesting, TestDeleteHead) {
	BOOL bRetVal = FALSE;

	bRetVal = ListDeleteHead(TestList);
	EXPECT_EQ(FALSE, bRetVal);

	ListInsertHead(TestList, (LPVOID)0);
	ListInsertHead(TestList, (LPVOID)1);
	ListInsertHead(TestList, (LPVOID)2);
	ListInsertHead(TestList, (LPVOID)3);
	ListInsertHead(TestList, (LPVOID)4);
	ListInsertHead(TestList, (LPVOID)5);
	ListInsertHead(TestList, (LPVOID)6);
	ListInsertHead(TestList, (LPVOID)7);

	bRetVal = ListDeleteHead(TestList);
	EXPECT_EQ(TRUE, bRetVal);
	EXPECT_EQ((SIZE_T)7, TestList->sztLen);

	bRetVal = ListDeleteHead(TestList);
	EXPECT_EQ(TRUE, bRetVal);
	EXPECT_EQ((SIZE_T)6, TestList->sztLen);

	bRetVal = ListDeleteHead(TestList);
	bRetVal = ListDeleteHead(TestList);
	bRetVal = ListDeleteHead(TestList);
	bRetVal = ListDeleteHead(TestList);
	bRetVal = ListDeleteHead(TestList);
	bRetVal = ListDeleteHead(TestList);
	EXPECT_EQ(TRUE, bRetVal);
	EXPECT_EQ((SIZE_T)0, TestList->sztLen);
	EXPECT_EQ(TRUE, ListIsEmpty(TestList));
	EXPECT_EQ(NULL, TestList->plnHead);
	EXPECT_EQ(NULL, TestList->plnTail);

	bRetVal = ListDeleteHead(TestList);
	EXPECT_EQ(FALSE, bRetVal);
}

TEST_F(DoublyLinkedListTesting, TestDeleteTail) {
	BOOL bRetVal = FALSE;

	bRetVal = ListDeleteTail(TestList);
	EXPECT_EQ(FALSE, bRetVal);

	ListInsertHead(TestList, (LPVOID)0);
	ListInsertHead(TestList, (LPVOID)1);
	ListInsertHead(TestList, (LPVOID)2);
	ListInsertHead(TestList, (LPVOID)3);
	ListInsertHead(TestList, (LPVOID)4);
	ListInsertHead(TestList, (LPVOID)5);
	ListInsertHead(TestList, (LPVOID)6);
	ListInsertHead(TestList, (LPVOID)7);

	bRetVal = ListDeleteTail(TestList);
	EXPECT_EQ(TRUE, bRetVal);
	EXPECT_EQ((SIZE_T)7, TestList->sztLen);

	bRetVal = ListDeleteTail (TestList);
	EXPECT_EQ(TRUE, bRetVal);
	EXPECT_EQ((SIZE_T)6, TestList->sztLen);

	bRetVal = ListDeleteTail(TestList);
	bRetVal = ListDeleteTail(TestList);
	bRetVal = ListDeleteTail(TestList);
	bRetVal = ListDeleteTail(TestList);
	bRetVal = ListDeleteTail(TestList);
	bRetVal = ListDeleteTail(TestList);
	EXPECT_EQ(TRUE, bRetVal);
	EXPECT_EQ((SIZE_T)0, TestList->sztLen);
	EXPECT_EQ(TRUE, ListIsEmpty(TestList));
	EXPECT_EQ(NULL, TestList->plnHead);
	EXPECT_EQ(NULL, TestList->plnTail);

	bRetVal = ListDeleteTail(TestList);

	EXPECT_EQ(FALSE, bRetVal);
}

TEST_F(DoublyLinkedListTesting, TestDeleteAll) {
	BOOL bRetVal = FALSE;

	bRetVal = ListDeleteAll(TestList);
	EXPECT_EQ(TRUE, bRetVal);

	ListInsertHead(TestList, (LPVOID)0);
	ListInsertHead(TestList, (LPVOID)1);
	ListInsertHead(TestList, (LPVOID)2);
	ListInsertHead(TestList, (LPVOID)3);
	ListInsertHead(TestList, (LPVOID)4);
	ListInsertHead(TestList, (LPVOID)5);
	ListInsertHead(TestList, (LPVOID)6);
	ListInsertHead(TestList, (LPVOID)7);
	
	bRetVal = ListDeleteAll(TestList);
	EXPECT_EQ(TRUE, bRetVal);
	EXPECT_EQ((SIZE_T)0, TestList->sztLen);
	EXPECT_EQ(TRUE, ListIsEmpty(TestList));
	EXPECT_EQ(NULL, TestList->plnHead);
	EXPECT_EQ(NULL, TestList->plnTail);

	bRetVal = ListDeleteAll(TestList);
	EXPECT_EQ(TRUE, bRetVal);
}